<?php


namespace MiCore\KernelTest;

use Doctrine\ORM\EntityManagerInterface;

class KernelTestCase extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();
        if (self::$container->has('doctrine')) {
            $this->entityManager = self::$container->get('doctrine')->getManager();
        }
    }

}
