<?php
namespace MiCore\KernelTest;

use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;

abstract class KernelTest extends Kernel
{
    use MicroKernelTrait;

    public function registerBundles(): iterable
    {

        $contents =  $this->bundles();
        $contents[] = FrameworkBundle::class;
        foreach ($contents as $class) {
            yield new $class();
        }
    }

    public abstract function configureContainer(ContainerBuilder $c);

    protected abstract function bundles(): array;

}
